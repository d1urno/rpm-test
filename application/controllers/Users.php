<?php
class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
		$this->load->helper('url_helper');

		// load form helper
		$this->load->helper(array('form'));

		// load form_validation library
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['users'] = $this->users_model->get_users();
		$data['title'] = 'Usuários Cadastrados';
		$this->load->view('templates/header', $data);
		$this->load->view('users/index', $data);
		$this->load->view('templates/footer');
	}

	public function view()
	{
		$data['users_item'] = $this->users_model->get_users();

		if (empty($data['users_item']))
		{
			show_404();
		}

		$data['title'] = $data['users_item']['name'];

		$this->load->view('templates/header', $data);
		$this->load->view('users/view', $data);
		$this->load->view('templates/footer');
	}

	public function create()
	{
		$data['title'] = 'Cadastro';

		$this->form_validation->set_rules('name', 'Nome', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('phone', 'Telefone', 'required|exact_length[15]|regex_match[/^((?![(]11[)]\s11111-1111).)*$/]');
		$this->form_validation->set_rules('password', 'Senha', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('repeatpassword', 'Confirmar senha', 'trim|required|matches[password]');

		$this->form_validation->set_message('required', 'O campo é obrigatório');
		$this->form_validation->set_message('min_length', 'O campo deve ter pelo menos 6 caracteres');
		$this->form_validation->set_message('valid_email', 'Digite um email válido');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('pages/cadastro', $data);
			$this->load->view('templates/footer', $data);
		}
		else
		{
			$this->users_model->set_users();
			$this->load->view('users/success');
		}
	}
}
