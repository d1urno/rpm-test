<div>
		<?php echo validation_errors(); ?>
	</div>

<script>jQuery(function($){
$("#phone").mask("(99) 99999-9999");
});
</script>

	<?php echo form_open('users/create'); ?>
		<div class="form-row">
			<div class="form-group col-md-5">
				<label for="name">Nome</label>
				<input type="text" class="form-control" name="name" required>
			</div>
			<div class="form-group col-md-5">
				<label for="email">Email</label>
				<input type="email" class="form-control" name="email" required>
			</div>
			<div class="form-group  col-md-5">
				<label for="phone">Telefone</label>
				<input type="tel" class="form-control" name="phone" id="phone" required>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-5">
				<label for="password">Senha</label>
				<input type="password" class="form-control" name="password" required>
			</div>
			<div class="form-group col-md-5">
				<label for="repeatpassword">Confirmar senha</label>
				<input type="password" class="form-control" name="repeatpassword" required>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-md-3">
				<label for="rg">RG (Opcional)</label>
				<input type="text" class="form-control" name="rg">
			</div>
<!--			<div class="form-group col-md-2">-->
<!--				<label for="cep">CEP</label>-->
<!--				<input type="text" class="form-control" name="cep">-->
<!--			</div>-->
		</div>
<!--		<div class="form-row">-->
<!--			<div class="form-group col-md-4">-->
<!--				<label for="city">Localidade</label>-->
<!--				<input type="text" class="form-control" id="city">-->
<!--			</div>-->
<!--			<div class="form-group col-md-2">-->
<!--				<label for="state">Uf</label>-->
<!--				<input type="text" class="form-control" id="state">-->
<!--			</div>-->
<!--			<div class="form-group col-md-4">-->
<!--				<label for="neighborhood">Bairro</label>-->
<!--				<input type="text" class="form-control" id="neighborhood">-->
<!--			</div>-->
<!--		</div>-->
		<button type="submit" name="submit" class="btn btn-primary">Cadastrar</button>
	</form>
