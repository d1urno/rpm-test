<table class="table">
	<thead>
	<tr>
		<th scope="col">#</th>
		<th scope="col">Nome</th>
		<th scope="col">Email</th>
		<th scope="col">Telefone</th>
		<th scope="col">RG</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$count = 1;
	foreach ($users as $users_item): ?>

		<tr>
			<th scope="row"><?php echo $count; $count++; ?></th>
			<td><?php echo $users_item['name']; ?></td>
			<td><?php echo $users_item['email']; ?></td>
			<td><?php echo $users_item['phone']; ?></td>
			<td><?php echo $users_item['rg']; ?></td>
		</tr>

	<?php endforeach; ?>
	</tbody>
</table>
