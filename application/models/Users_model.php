<?php
class Users_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_users()
	{
		$query = $this->db->get('users');
		return $query->result_array();
	}

	public function set_users()
	{
		$this->load->helper('url');

		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'password' => $this->input->post('password'),
			'rg' => $this->input->post('rg'),
		);

		return $this->db->insert('users', $data);
	}
}
